---
title: <% tp.date.now() %>
alias: Daily Note - <% tp.date.now() %>
tags: ["calendar/daily"] 
created: "<% tp.date.now("") %>"
updated: 
---


[[<% tp.date.yesterday() %>]] «  »  [[<% tp.date.tomorrow() %>]]

# Morning Pages

## Brain dump
[[<%(await tp.file.create_new(tp.file.find_tfile('Template, Daily Note Brain Dump'), `${tp.date.now()}, Braindump`)).basename %>]]

## Gratitude
[[<%(await tp.file.create_new(tp.file.find_tfile('Template, Daily Note Gratitude'), `${tp.date.now()}, Gratitude`)).basename %>]]

## Review
[[<%(await tp.file.create_new(tp.file.find_tfile('Template, Daily Note Review'), `${tp.date.now()}, Review`)).basename %>]]
## Goals
[[<%(await tp.file.create_new(tp.file.find_tfile('Template, Daily Note Goals'), `${tp.date.now()}, Goals`)).basename %>]]

## Service to others
[[<%(await tp.file.create_new(tp.file.find_tfile('Template, Daily Note Service to others'), `${tp.date.now()}, Service to others`)).basename %>]]

# Interstitial Notes
[[<%(await tp.file.create_new(tp.file.find_tfile('Template, Daily Note Interstitial Notes'), `${tp.date.now()}, Interstitial Notes`)).basename %>]]

# Dev Log
[[<%(await tp.file.create_new(tp.file.find_tfile('Template, Daily Note Dev Log'), `${tp.date.now()}, Devlog`)).basename %>]]

# Scratchpad
[[<%(await tp.file.create_new(tp.file.find_tfile('Template, Daily Note Scratchpad'), `${tp.date.now()}, Scratchpad`)).basename %>]]


