---
title: <% tp.date.now() %>, Review
aliases: ["Daily Review Note - <% tp.date.now() %>", "Review of what happened today and yesterday - <% tp.date.now() %>"]
tags: ["calendar/daily/review"] 
created: "<% tp.date.now("") %>"
updated: 
---

up:: [[<% tp.date.now() %>]]

# Review