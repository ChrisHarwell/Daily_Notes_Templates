---
title: <% tp.date.now() %>, Service to others
aliases: ["Daily Service to others Note - <% tp.date.now() %>", "How can I be of service to others today - <% tp.date.now() %>"]
tags: ["calendar/daily/service"] 
created: "<% tp.date.now("") %>"
updated: 
---

up:: [[<% tp.date.now() %>]]

# How Can I serve others today?
## What is one thing I can do today to show [[@Kaylee Harwell|my wife]] love with my words?
- 

## What is one thing I can do today to show [[@Kaylee Harwell|my wife]] love with my actions?
-  