---
title: <% tp.date.now() %>, Interstitial Notes
aliases: ["Daily Interstitial Notes - <% tp.date.now() %>", "Interstitial Notes for Today - <% tp.date.now() %>"]
tags: ["calendar/daily/interstitial"] 
created: "<% tp.date.now("") %>"
updated: 
---

up:: [[<% tp.date.now() %>]]

# Interstitial Notes
<span style='color:#20bf6b'>Time</span> -