---
title: <% tp.date.now() %>, Scratchpad
aliases: ["Daily Scratchpad - <% tp.date.now() %>", "Scratchpad for Today - <% tp.date.now() %>"]
tags: ["calendar/daily/scratchpad"] 
created: "<% tp.date.now("") %>"
updated: 
---

up:: [[<% tp.date.now() %>]]

# Scratchpad
