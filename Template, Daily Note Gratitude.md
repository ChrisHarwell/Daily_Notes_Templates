---
title: <% tp.date.now() %>, Gratitude
aliases: ["Daily Gratitude Note - <% tp.date.now() %>", "Things I am grateful for today - <% tp.date.now() %>"]
tags: ["calendar/daily/gratitude"] 
created: "<% tp.date.now("") %>"
updated: 
---

up:: [[<% tp.date.now() %>]]
# Three things I am grateful for today
1. 
2. 
3. 