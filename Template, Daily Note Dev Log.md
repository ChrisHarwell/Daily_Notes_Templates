---
title: <% tp.date.now() %>, Dev Log
aliases: ["Daily Dev Log - <% tp.date.now() %>", "Dev Log for Today - <% tp.date.now() %>"]
tags: ["calendar/daily/devLog"] 
created: "<% tp.date.now("") %>"
updated: 
---

up:: [[<% tp.date.now() %>]]

# Dev Log
## Brain Dump

## Scratchpad

## Interstitial Notes
<span style='color:#20bf6b'>Time</span> -