---
title: <% tp.date.now() %>, Goals
aliases: ["Daily Goal Note - <% tp.date.now() %>", "Today's Goals - <% tp.date.now() %>"]
tags: ["calendar/daily/goals"] 
created: "<% tp.date.now("") %>"
updated: 
---

up:: [[<% tp.date.now() %>]]

# Today's Goals
### Highest Priority Action Item
1. 

### Top 5 Action Items
1. 
2. 
3. 
4. 
5. 